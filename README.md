Sushi
=====

A Ruby script which generates a C99 component stub; documentation
in DocBook, unit-tests with CUnit.  Examples can be found [here][1].

[1]: https://jjg.gitlab.io/en/code/c99-sushi/
